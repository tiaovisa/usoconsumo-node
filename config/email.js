var MailListener = require('mail-listener2');
 var nota = require('../config/notas');
 var credenciais = {
    email : 'lucas.mezuraro@acovisa.com.br',
    senha : 'aco1611',
    servidor : 'zimbra.acovisa.com.br',
    porta : '143'
 };
 exports.Credenciais = function (retorno) {
    if (retorno) {
        credenciais.email = retorno.email;
        credenciais.senha = retorno.senha;
        credenciais.servidor = retorno.servidor;
        credenciais.porta = retorno.porta;
    }
    devolveLogin(retorno);
    return credenciais;
 }
 
function devolveLogin(dados) {
    credenciais.email = dados.email;
    credenciais.senha = dados.senha;
    credenciais.servidor = dados.servidor;
    credenciais.porta = dados.porta;   
    return dados;
}

function devolveDados() {
    return credenciais;
}

 exports.object = new MailListener({
        username: credenciais.email,
        password: credenciais.senha,
        host: credenciais.servidor,
        port: credenciais.porta, // imap port
        tls: false,
        strictSSL: false,
        tlsOptions: {rejectUnauthorized: true},
        mailbox: "INBOX", // mailbox to monitor
        searchFilter: ["UNSEEN"], // the search filter being used after an IDLE notification has been retrieved
        markSeen: true, // all fetched email willbe marked as seen and not fetched next time
        fetchUnreadOnStart: true, // use it only if you want to get all unread email on lib start. Default is `false`,
        mailParserOptions: {streamAttachments: true}, // options to be passed to mailParser lib.
        attachments: true // download attachments as they are encountered to the project directory
       // attachmentOptions: {directory: "C:/users/lucas.mezuraro/desktop/servidor"} // specify a download directory for attachments
  
  });


