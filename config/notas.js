var leitorXml = require('../xml/leituraXml');
var driver = require('../bancoDados/cache');
var cache = new driver.Cache();
var instancia = require('../bancoDados/instancia');
function conectaCache() {
     cache.open(instancia.credenciais(), function (erro, resultado) {});
  }
conectaCache();
exports.credenciaisEmail = function (instanciaBanco) {
    var tabela = {
        class : 'UsoConsumo.CONFIGURACAOEMAIL',
        method : 'devolveCredenciais',
        arguments : []
    };
    var cred = [];
  return new Promise (function (sucess, reject) {
    cache.invoke_classmethod(tabela,function (err, result) {
        var resultado = result.result;
        var array = resultado.split("^");
        cred = {
           email: array[0],
           senha: array[1],
           servidor: array[2],
           porta: array[3]        
        };

        sucess(cred);
    });
  });  
     
}

exports.pegaNota = function(objeto) {
  var fornecId = "";
  var atribFornec = leitorXml.pegaAtributosFornecedor(objeto);
  var atribNF = leitorXml.pegaAtributosNotaFiscal(objeto);
  var atribDest = leitorXml.pegaAtributosDestinario(objeto);
  var atribPed = leitorXml.pegaAtributosItens(objeto);

  var tabela =  {
    class: "UsoConsumo.CONSUMOFORNECEDOR",
    method: "verificar",
    arguments: [atribFornec[1]]
  };
  function obtemBuscaFornecedor(retorno) {  
      var nf = 0; 
      cache.invoke_classmethod(tabela, function (err, result) {
          retorno(result);
      });
  }
 
  var fl = new Promise(function (success, reject) {
    
    /* PRIMEIRA ETAPA BUSCA O CRIA FORNECEDOR */

    obtemBuscaFornecedor(function (retorno) {
      var retornoFornecedor = 0;
      var validacaoFornecedor = retorno.result;
      if (retorno.result == 0) {
          var tabela =  {
              class: "UsoConsumo.CONSUMOFORNECEDOR",
                method: "cadastroFornecedor",
                arguments: atribFornec
              };
            cache.invoke_classmethod(tabela, function (err, result) {
        
            if (result.result > 0) {
                fornec = result.result; 
                success(fornec);
                console.log('Criou Fornecedor');
            }else {
                fornec = retorno.result;
                success(fornec);
                console.log('Buscou Fornecedor');
            }
          });
        }else {
          fornec = retorno.result;
          success(fornec);
        }
     });
  });

  fl.then(function (fornec) {
    /* SEGUNDA ETAPA CRIA A NOTA FISCAL */
    
    return new Promise(function (sucesso, rejeicao) {
        atribNF.push(fornec);
        var tabela =  {
              class: "UsoConsumo.CONSUMONOTAFISCAL",
                  method: "criaNF",
                  arguments: atribNF
              };
          cache.invoke_classmethod(tabela, function(err, result) {
            numeroNF = result.result;
            console.log(result);
            sucesso(numeroNF);
            if (result.result > 0) {
                    console.log('Nota Fiscal: '+result.result);
            }else {
              console.log('=>>'+atribNF);
            }
          });
    });
  }).then(function (numNF) {
    /* TERCEIRA ETAPA CRIA PEDIDO DE COMPRA */
    
    return new Promise(function (success, rejeict) {
            if (numeroNF > 0) {
                var numPedComp = '';
                  if (atribPed[1] instanceof Array) {
                      numPedComp = atribPed[1][9];
                  }else {
                      numPedComp = atribPed[0][9];
                  }

                  if (!numPedComp) {
                    numPedComp = 0;
                    console.log('FALHA NA BUSCA DO PEDIDO DE COMPRA: ENVIAR EMAIL!');
                  }

                var tabela =  {
                    class: "UsoConsumo.CONSUMOPEDIDOCOMPRA",
                    method: "criaPedido",
                    arguments: [numeroNF,fornec, numPedComp]
                };

              cache.invoke_classmethod(tabela, function(err, result) {
                  pedidoCompra = result.result;
                  success(pedidoCompra);
                    if (result.result > 0) {
                        console.log('Pedido '+result.result);
                    }
              });
          }
      });
  }).then(function (pedidoComp) {
    
    /* QUARTA ETAPA CRIA OS ITENS DO PEDIDO */
      
      return new Promise(function (success, reject) {
        if (pedidoComp > 0) {
            var cont = 0;
          for(var i = 0; i < atribPed.length; i++) {
              atribPed[i].push(pedidoComp);
              var tabela =  {
                  class: "UsoConsumo.CONSUMOPEDIDOITENS",
                  method: "criaItens",
                  arguments: atribPed[i]
              };
              cache.invoke_classmethod(tabela, function (err, result) {
                pedidoItens = result.result;
              
                if (result.result == 1) {
                  cont++;
                  console.log('Itens Salvos!');
                }else {
                  console.log(result);
                }
              });
            }
        }
        success(1);
     });    
          
  }).then(function (retornoFinal) {
      var tabela =  {
                  class: "UsoConsumo.Integracao",
                  method: "atualizaNF",
                  arguments: [numeroNF]
              };
     cache.invoke_classmethod(tabela,function (err, result) {
        if (result.result == "1") {
          console.log('Nota integrada ao RM!');
        }else {
          console.log('Problema detectado!');
          console.log(result.result);
        }
     });
        
    
    console.log('--------------------------------------');
    console.log('\n');
  });
}