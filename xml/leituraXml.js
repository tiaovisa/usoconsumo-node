var fs = require('fs');
var moment = require('moment');
      exports.deserializaXMLConsumo = function(nomeXML) {
        console.log('NOME: '+nomeXML);
        return fs.readFileSync(nomeXML, 'utf-8');
      }

     exports.pegaAtributosNotaFiscal = function(data) {
        var xml2js = require('xml2js');
        var atributos;
        if (data) {
                var xmlDoc = data.toString();
                var parser = new xml2js.Parser({explicitArray : false});
                parser.parseString(xmlDoc, function(err,result) {
                  //var cont = true;
                  if (err) {
                    console.log(err);
                  }else {
                    var conteudo = result.nfeProc.NFe.infNFe;
                    var numeroNF = result.nfeProc.NFe.infNFe.ide.nNF;
                    var serie = result.nfeProc.NFe.infNFe.ide.serie;
                    var dataHoraEmissao = result.nfeProc.NFe.infNFe.ide.dhEmi;
                    var dataHoraSaidaEntrada = result.nfeProc.NFe.infNFe.ide.dhSaiEnt;
                    var valorTotal = result.nfeProc.NFe.infNFe.total.ICMSTot.vNF;
                    var valorDesconto = result.nfeProc.NFe.infNFe.total.ICMSTot.vDesc;
                    var valorFrete = result.nfeProc.NFe.infNFe.total.ICMSTot.vFrete;
                    var valorPis = result.nfeProc.NFe.infNFe.total.ICMSTot.vPIS;
                    var valorConfins = result.nfeProc.NFe.infNFe.total.ICMSTot.vCOFINS;
                    var valorICMS = result.nfeProc.NFe.infNFe.total.ICMSTot.vICMS;
                    var tipoTransporte = result.nfeProc.NFe.infNFe.transp.modFrete;
                    var dataEmissao = dataHoraEmissao.split('T'); 
                    var devolveData = moment(dataEmissao[0], "YYYY-MM-DD");
                    var dataEmissao = devolveData.format('DD/MM/YYYY');
                  
            
                    atributos = [
                        numeroNF,
                        dataEmissao,
                        serie,
                        valorTotal,
                        valorPis,
                        valorTotal,
                        valorPis,
                        valorConfins,
                        0,
                        0,
                        '3',
                        valorDesconto,
                        valorICMS
                    ];
              }
          });
        }        
          return atributos;
      }

        exports.pegaAtributosFornecedor = function(data) {
        var xml2js = require('xml2js');
        var atributos;
        if (data) {
          var xmlDoc = data.toString();
                var parser = new xml2js.Parser({explicitArray : false});
                parser.parseString(xmlDoc, function(err,result) {
                  //var cont = true;
                  if (err) {
                    console.log(err);
                  }else {
              var cnpj = result.nfeProc.NFe.infNFe.emit.CNPJ;
              var razaoSocial = result.nfeProc.NFe.infNFe.emit.xNome;
              var nomeFantasia = result.nfeProc.NFe.infNFe.emit.xFant;
                    var endereco = result.nfeProc.NFe.infNFe.emit.enderEmit.xLgr;
                    var endNumero = result.nfeProc.NFe.infNFe.emit.enderEmit.nro;
                    var endBairro = result.nfeProc.NFe.infNFe.emit.enderEmit.xBairro;
                    var codMunicipio = result.nfeProc.NFe.infNFe.emit.enderEmit.cMun;
                    var endMunicipio = result.nfeProc.NFe.infNFe.emit.enderEmit.xMun;
                    var endUF = result.nfeProc.NFe.infNFe.emit.enderEmit.UF;
                    var endCEP = result.nfeProc.NFe.infNFe.emit.enderEmit.CEP;
                    var codPais = result.nfeProc.NFe.infNFe.emit.enderEmit.cPais;
                    var endPais = result.nfeProc.NFe.infNFe.emit.enderEmit.xPais;
                    var endTelefone = result.nfeProc.NFe.infNFe.emit.enderEmit.fone;
                    var fornecedorIE = result.nfeProc.NFe.infNFe.emit.IE;

                    atributos = [
                          razaoSocial,
                          cnpj,
                          endereco,
                          endNumero,
                          endCEP,
                          endBairro,
                          fornecedorIE,
                          endMunicipio,
                          endUF,
                          endTelefone
                    ];
              }
          });
        }
         return atributos;
      }

       exports.pegaAtributosDestinario = function(data) {
        var xml2js = require('xml2js');
        var atributos;
        if (data) {
          var xmlDoc = data.toString();

                var parser = new xml2js.Parser({explicitArray : false});
                parser.parseString(xmlDoc,function (err,result) {
                  //var cont = true;
                  if (err) {
                    console.log(err);
                  }else {
              var cnpj = result.nfeProc.NFe.infNFe.dest.CNPJ;
              var razaoSocial = result.nfeProc.NFe.infNFe.dest.xNome;
                    var endereco = result.nfeProc.NFe.infNFe.dest.enderDest.xLgr;
                    var endNumero = result.nfeProc.NFe.infNFe.dest.enderDest.nro;
                    var endBairro = result.nfeProc.NFe.infNFe.dest.enderDest.xBairro;
                    var codMunicipio = result.nfeProc.NFe.infNFe.dest.enderDest.cMun;
                    var endMunicipio = result.nfeProc.NFe.infNFe.dest.enderDest.xMun;
                    var endUF = result.nfeProc.NFe.infNFe.dest.enderDest.UF;
                    var endCEP = result.nfeProc.NFe.infNFe.dest.enderDest.CEP;
                    var codPais = result.nfeProc.NFe.infNFe.dest.enderDest.cPais;
                    var endPais = result.nfeProc.NFe.infNFe.dest.enderDest.xPais;
                    var endTelefone = result.nfeProc.NFe.infNFe.dest.enderDest.fone;
                    var fornecedorIE = result.nfeProc.NFe.infNFe.dest.IE;

                    atributos = [
                          cnpj,
                          razaoSocial,
                          endereco,
                          endNumero,
                          endBairro,
                          codMunicipio,
                          endMunicipio,
                          endUF,
                          endCEP,
                          codPais,
                          endPais,
                          endTelefone,
                          fornecedorIE
                    ];
              }
          });
        }        
         return atributos;
      }

     exports.pegaAtributosItens = function(data) {
      var xml2js = require('xml2js');
      var itensNF = [];
      var atributos;
      if (data) {
        var xmlDoc = data.toString();
              var parser = new xml2js.Parser({explicitArray : false});
              parser.parseString(xmlDoc,function (err,result) {
                if (err) {
                  console.log(err);
                }else {
            var cnpj = result.nfeProc.NFe.infNFe.dest.CNPJ;
            var razaoSocial = result.nfeProc.NFe.infNFe.dest.xNome;
                  var endereco = result.nfeProc.NFe.infNFe.dest.enderDest.xLgr;
                  var endNumero = result.nfeProc.NFe.infNFe.dest.enderDest.nro;
                  var endBairro = result.nfeProc.NFe.infNFe.dest.enderDest.xBairro;
                  var codMunicipio = result.nfeProc.NFe.infNFe.dest.enderDest.cMun;
                  var endMunicipio = result.nfeProc.NFe.infNFe.dest.enderDest.xMun;
                  
                  var endUF = result.nfeProc.NFe.infNFe.dest.enderDest.UF;
                  var endCEP = result.nfeProc.NFe.infNFe.dest.enderDest.CEP;
                  var codPais = result.nfeProc.NFe.infNFe.dest.enderDest.cPais;
                  var endPais = result.nfeProc.NFe.infNFe.dest.enderDest.xPais;
                  var endTelefone = result.nfeProc.NFe.infNFe.dest.enderDest.fone;
                  var fornecedorIE = result.nfeProc.NFe.infNFe.dest.IE;

              var itens = result.nfeProc.NFe.infNFe.det;
            if (itens.length != undefined) {
              for (var i = 0; i < itens.length; i++) {
                
                var item = [];

                  var numeroItem = i+1;
                  var produto = result.nfeProc.NFe.infNFe.det[i].prod;
                  var codigoProduto = produto.cProd;
                  var prodDescricao = produto.xProd;
                  var classificacaoFiscal = produto.NCM;
                  var unidadeCompra = produto.uCom;
                  var quantidadeCompra = produto.uCom;
                  var quantidadeProduto = produto.qCom;
                  var valorUnidadeCompra = produto.vUnCom;
                  var valorCompra = produto.vProd;
                  var numeroPedido = produto.xPed;
                  var produtoCFOP = produto.CFOP;
                  var impostoProduto = result.nfeProc.NFe.infNFe.det[i].imposto;
                  
                  /* IMPOSTO ICMS */

                  var impostoProdutoICMS = impostoProduto.ICMS;
                  if (impostoProdutoICMS.ICMS00 != undefined) {
                    var impostoProdutoICMS = impostoProdutoICMS.ICMS00;
                    var valorBaseICMS = ((impostoProdutoICMS.vBC) + 0);
                    var percentualICMSProduto = ((impostoProdutoICMS.pICMS) + 0);
                    var valorICMSProduto = ((impostoProdutoICMS.vICMS) + 0);
                    var ICMSProdutoCST = impostoProdutoICMS.CST;
                  }else if (impostoProdutoICMS.ICMS60 != undefined) {
                    var impostoProdutoICMS = impostoProdutoICMS.ICMS60;
                    var valorBaseICMS = 0;
                    var percentualICMSProduto = 0;
                    var valorICMSProduto = 0;
                    var ICMSProdutoCST = impostoProdutoICMS.CST;
                  }else if (impostoProdutoICMS.ICMSSN500 != undefined) {
                    var impostoProdutoICMS = impostoProdutoICMS.ICMSSN500;
                    var valorBaseICMS = ((impostoProdutoICMS.vBCSTRet) + 0);
                    var percentualICMSProduto = 0;
                    var valorICMSProduto = ((impostoProdutoICMS.vICMSSTRet) + 0);
                    var ICMSProdutoCST = impostoProdutoICMS.CSOSN;
                  }else if (impostoProdutoICMS.ICMSSN101 != undefined) {
                    var impostoProdutoICMS = impostoProdutoICMS.ICMSSN101;
                    var valorBaseICMS = 0;
                    var percentualICMSProduto = ((impostoProdutoICMS.pCredSN) + 0);
                    var valorICMSProduto = ((impostoProdutoICMS.vCredICMSSN) + 0);
                    var ICMSProdutoCST = impostoProdutoICMS.CSOSN;
                  }else if (impostoProdutoICMS.ICMS90 != undefined) {
                    var impostoProdutoICMS = impostoProdutoICMS.ICMS90;
                    var valorBaseICMS = 0;
                    var percentualICMSProduto = ((impostoProdutoICMS.pICMS) + 0);
                    var valorICMSProduto = ((impostoProdutoICMS.vICMS) + 0);
                    var ICMSProdutoCST = impostoProdutoICMS.CST;
                  }else {
                    var valorBaseICMS = 0;
                    var percentualICMSProduto = 0;
                    var valorICMSProduto = 0;
                    var ICMSProdutoCST = 0;
                  }
                  var percentualICMSProduto = ((percentualICMSProduto) + 0);
                  /* IMPOSTO IPI */

                  var impostoProdutoIPI = impostoProduto.IPI;
                 if (impostoProduto.IPI != undefined) {
                        if (impostoProdutoIPI.IPITrib != undefined) {
                          var impostoProdutoIPI = impostoProdutoIPI.IPITrib;
                          var IPIProdutoCST = impostoProdutoIPI.CST;
                           var percentualProdutoIPI = (impostoProdutoIPI.pIPI) + 0;
                          if (impostoProdutoIPI.pIPI != undefined) {
                            var percentualProdutoIPI = (impostoProdutoIPI.pIPI) + 0;
                          }else {
                            var percentualProdutoIPI = 0;
                          }
                        }else if (impostoProdutoIPI.IPINT != undefined) {
                          var impostoProdutoIPI = impostoProdutoIPI.IPINT;
                          if (impostoProdutoIPI.pIPI != undefined) {
                            var percentualProdutoIPI = (impostoProdutoIPI.pIPI) + 0;
                          }else {
                            var percentualProdutoIPI = 0;
                          }
                          var IPIProdutoCST = impostoProdutoIPI.CST;
                        }else {
                          var IPIProdutoCST = 0;
                          var percentualProdutoIPI = 0;
                        }
                      }else {
                        var IPIProdutoCST = 0;
                        var percentualProdutoIPI = 0;
                      }
                 
                 /* 
                    var IPIProdutoCST = impostoProdutoIPI.CST;
                    var percentualProdutoIPI = impostoProdutoIPI.pIPI;
                    var valorProdutoIPI = impostoProdutoIPI.vIPI;
                  */

                  /* IMPOSTO PIS */

                  var impostoProdutoPIS = impostoProduto.PIS;
                  if (impostoProdutoPIS != undefined) {
                    if (impostoProdutoPIS.PISAliq != undefined) {
                      var impostoProdutoPIS = impostoProdutoPIS.PISAliq;
                      var valorPISBase = ((impostoProdutoPIS.vBC) + 0);
                      var percentualPISProduto = ((impostoProdutoPIS.pPIS) + 0);
                      var valorPISProduto = ((impostoProdutoPIS.vPIS) + 0);
                      var PISProdutoCST = impostoProdutoPIS.CST;
                    }else if (impostoProdutoPIS.PISOutr != undefined) {
                       var impostoProdutoPIS = impostoProdutoPIS.PISOutr;
                       var valorPISBase = ((impostoProdutoPIS.vBC) + 0);
                       var percentualPISProduto = ((impostoProdutoPIS.pPIS) + 0);
                       var valorPISProduto = ((impostoProdutoPIS.vPIS) + 0);
                       var PISProdutoCST = impostoProdutoPIS.CST;
                    }else {
                       var valorPISBase = 0;
                       var percentualPISProduto = 0;
                       var valorPISProduto = 0;
                       var PISProdutoCST = 0;
                    }
                  }
                  /* IMPOSTO COFINS */
                  var impostoProdutoCOFINS = impostoProduto.COFINS;
                  if (impostoProdutoCOFINS != undefined) {
                    if (impostoProdutoCOFINS.COFINSAliq != undefined) {
                      var impostoProdutoCOFINS = impostoProdutoCOFINS.COFINSAliq;
                      var valorCOFINSBase = ((impostoProdutoCOFINS.vBC) + 0);
                      var percentualCOFINSProduto = ((impostoProdutoCOFINS.pCOFINS) + 0);
                      var valorCOFINSProduto = ((impostoProdutoCOFINS.vCOFINS) + 0);
                      var COFINSCst = impostoProdutoCOFINS.CST;
                    }else if (impostoProdutoCOFINS.COFINSOutr != undefined) {
                      var impostoProdutoCOFINS = impostoProdutoCOFINS.COFINSOutr;
                      var valorCOFINSBase = (impostoProdutoCOFINS.vBC + 0);
                      var percentualCOFINSProduto = ((impostoProdutoCOFINS.pCOFINS) + 0);
                      var valorCOFINSProduto = ((impostoProdutoCOFINS.vCOFINS) + 0);
                      var COFINSCst = impostoProdutoCOFINS.CST;
                    }else {
                      var valorCOFINSBase = 0;
                      var percentualCOFINSProduto = 0;
                      var valorCOFINSProduto = 0;
                    }
                    
                  }else {
                    var valorCOFINSBase = 0;
                    var percentualCOFINSProduto = 0;
                    var valorCOFINSProduto = 0;
                  }
                  if (!numeroPedido) {
                    var numeroPedido = 0;
                  }

                  item = [
                    prodDescricao,
                    valorUnidadeCompra,
                    classificacaoFiscal,
                    codigoProduto,
                    valorCompra,
                    1,
                    valorICMSProduto,
                    valorPISProduto,
                    0,
                    numeroPedido,
                    quantidadeProduto,
                    produtoCFOP,
                    IPIProdutoCST,
                    ICMSProdutoCST,
                    COFINSCst,
                    PISProdutoCST,
                    percentualICMSProduto,
                    percentualProdutoIPI,
                    percentualCOFINSProduto,
                    percentualPISProduto               
                  ];
                itensNF.push(item);
              }

            }else {
              
              var item = [];

              var numeroItem = 1;
              var produto = result.nfeProc.NFe.infNFe.det.prod;
              var codigoProduto = produto.cProd;
              var prodDescricao = produto.xProd;
              var classificacaoFiscal = produto.NCM;
              var produtoCFOP = produto.CFOP;
              var unidadeCompra = produto.uCom;
              var quantidadeCompra = produto.uCom;
              var quantidadeProduto = produto.qCom;
              var valorUnidadeCompra = produto.vUnCom;
              var valorCompra = produto.vProd;
              var numeroPedido = produto.xPed;
              var impostoProduto = result.nfeProc.NFe.infNFe.det.imposto;
              
              /* IMPOSTO ICMS */
              var impostoProdutoICMS = impostoProduto.ICMS;

              if (impostoProdutoICMS.ICMS00 != undefined) {
                    var impostoProdutoICMS = impostoProdutoICMS.ICMS00;
                    var valorBaseICMS = ((impostoProdutoICMS.vBC) + 0);
                    var percentualICMSProduto = ((impostoProdutoICMS.pICMS) + 0);
                    var valorICMSProduto = ((impostoProdutoICMS.vICMS) + 0);
                    var ICMSProdutoCST = impostoProdutoICMS.CST;
             }else if (impostoProdutoICMS.ICMS60 != undefined) {
              var impostoProdutoICMS = impostoProdutoICMS.ICMS60;
              var valorBaseICMS = 0;
              var percentualICMSProduto = 0;
              var valorICMSProduto = 0;
              var ICMSProdutoCST = impostoProdutoICMS.CST;
            }else if (impostoProdutoICMS.ICMSSN500 != undefined) {
              var impostoProdutoICMS = impostoProdutoICMS.ICMSSN500;
              var valorBaseICMS = ((impostoProdutoICMS.vBCSTRet) + 0);
              var percentualICMSProduto = 0;
              var valorICMSProduto = ((impostoProdutoICMS.vICMSSTRet) + 0);
              var ICMSProdutoCST = impostoProdutoICMS.CSOSN;
            }else if (impostoProdutoICMS.ICMSSN101 != undefined) {
              var impostoProdutoICMS = impostoProdutoICMS.ICMSSN101;
              var valorBaseICMS = 0;
              var percentualICMSProduto = ((impostoProdutoICMS.pCredSN) + 0);
              var valorICMSProduto = ((impostoProdutoICMS.vCredICMSSN) + 0);
              var ICMSProdutoCST = impostoProdutoICMS.CSOSN;
            }else if (impostoProdutoICMS.ICMS90 != undefined) {
              var impostoProdutoICMS = impostoProdutoICMS.ICMS90;
              var valorBaseICMS = 0;
              var percentualICMSProduto = ((impostoProdutoICMS.pICMS) + 0);
              var valorICMSProduto = ((impostoProdutoICMS.vICMS) + 0);
              var ICMSProdutoCST = impostoProdutoICMS.CST;
            }else {
              var valorBaseICMS = 0;
              var percentualICMSProduto = 0;
              var valorICMSProduto = 0;
              var ICMSProdutoCST = 0;
            }
                    /* IMPOSTO IPI */
                    var impostoProdutoIPI = impostoProduto.IPI;                
                   if (impostoProduto.IPI != undefined) {
                        if (impostoProdutoIPI.IPITrib != undefined) {
                          var impostoProdutoIPI = impostoProdutoIPI.IPITrib;
                          var IPIProdutoCST = impostoProdutoIPI.CST;
                          if (impostoProdutoIPI.pIPI != undefined) {
                            var percentualProdutoIPI = (impostoProdutoIPI.pIPI) + 0;
                          }else {
                            var percentualProdutoIPI = 0;
                          }
                        }else if (impostoProdutoIPI.IPINT != undefined) {
                          var impostoProdutoIPI = impostoProdutoIPI.IPINT;
                          var IPIProdutoCST = impostoProdutoIPI.CST;
                          if (impostoProdutoIPI.pIPI != undefined) {
                            var percentualProdutoIPI = (impostoProdutoIPI.pIPI) + 0;
                          }else {
                            var percentualProdutoIPI = 0;
                          }
                        }else {
                          var IPIProdutoCST = 0;
                          var percentualProdutoIPI = 0;
                        }
                      }else {
                        var IPIProdutoCST = 0;
                        var percentualProdutoIPI = 0;
                      }

                    /* IMPOSTO PIS */
                    var impostoProdutoPIS = impostoProduto.PIS;
                    if (impostoProdutoPIS != undefined) {
                    if (impostoProdutoPIS.PISAliq != undefined) {
                      var impostoProdutoPIS = impostoProdutoPIS.PISAliq;
                      var valorPISBase = ((impostoProdutoPIS.vBC) + 0);
                      var percentualPISProduto = ((impostoProdutoPIS.pPIS) + 0);
                      var valorPISProduto = ((impostoProdutoPIS.vPIS) + 0);
                      var PISProdutoCST = impostoProdutoPIS.CST;
                    }else if (impostoProdutoPIS.PISOutr != undefined) {
                       var impostoProdutoPIS = impostoProdutoPIS.PISOutr;
                       var valorPISBase = ((impostoProdutoPIS.vBC) + 0);
                       var percentualPISProduto = ((impostoProdutoPIS.pPIS) + 0);
                       var valorPISProduto = ((impostoProdutoPIS.vPIS) + 0);
                       var PISProdutoCST = impostoProdutoPIS.CST;
                    }else {
                       var valorPISBase = 0;
                       var percentualPISProduto = 0;
                       var valorPISProduto = 0;
                       var PISProdutoCST = 0;
                    }
                  }
                    /* IMPOSTO COFINS */
                  var impostoProdutoCOFINS = impostoProduto.COFINS;
                  if (impostoProdutoCOFINS != undefined) {
                    if (impostoProdutoCOFINS.COFINSAliq != undefined) {
                      var impostoProdutoCOFINS = impostoProdutoCOFINS.COFINSAliq;
                      var valorCOFINSBase = (impostoProdutoCOFINS.vBC + 0);
                      var percentualCOFINSProduto = (impostoProdutoCOFINS.pCOFINS) + 0;
                      var valorCOFINSProduto = (impostoProdutoCOFINS.vCOFINS + 0);
                      var COFINSCst = impostoProdutoCOFINS.CST;
                    }else if (impostoProdutoCOFINS.COFINSOutr != undefined) {
                      var impostoProdutoCOFINS = impostoProdutoCOFINS.COFINSOutr;
                      var valorCOFINSBase = ((impostoProdutoCOFINS.vBC) + 0);
                      var percentualCOFINSProduto = ((impostoProdutoCOFINS.pCOFINS) + 0);
                      var valorCOFINSProduto = ((impostoProdutoCOFINS.vCOFINS) + 0);
                      var COFINSCst = impostoProdutoCOFINS.CST;
                    }else {
                      var valorCOFINSBase = 0;
                      var percentualCOFINSProduto = 0;
                      var valorCOFINSProduto = 0;
                      var COFINSCst = 0;
                    }
                    
                  }else {
                    var valorCOFINSBase = 0;
                    var percentualCOFINSProduto = 0;
                    var valorCOFINSProduto = 0;
                  }
                  if(!numeroPedido){
                    var numeroPedido = 0;
                  }

                  //console.log(valorCOFINSProduto);
                  //console.log(impostoProdutoCOFINS.vCOFINS);
              item = [
                    prodDescricao,
                    valorUnidadeCompra,
                    classificacaoFiscal,
                    codigoProduto,
                    valorCompra,
                    1,
                    valorICMSProduto,
                    valorPISProduto,
                    0,
                    numeroPedido,
                    quantidadeProduto,
                    produtoCFOP,
                    IPIProdutoCST,
                    ICMSProdutoCST,
                    COFINSCst,
                    PISProdutoCST,
                    percentualICMSProduto,
                    percentualProdutoIPI,
                    percentualCOFINSProduto,
                    percentualPISProduto               
                  ];
                itensNF.push(item);
            }

            }

            
        });
      }
       return itensNF;
    }
  
